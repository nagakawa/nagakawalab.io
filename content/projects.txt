## [AGL / TDR](https://gitlab.com/nagakawa/tdr)

**Note:** The `refactor-tdr` branch is where all development is currently occurring. It is recommended to use this branch (instead of the `master` branch) to make it easier to migrate once it's merged into master.

* A danmaku engine.
* More precisely, **AGL** is the general game library and **TDR** is for danmaku-specific stuff.
* [API docs](https://nagakawa.gitlab.io/tdr/index.html) for the `refactor-tdr` branch.
* [Roadmap](https://gitlab.com/nagakawa/tdr/wikis/TDR-Roadmap) for the future.

## [VaneBot](https://github.com/nagakawa/vane)

* Discord bot made exclusively for the AGC server
* [Now written in Rust!](https://gitlab.com/nagakawa/vane-rs)
* Formerly written in C++ (using a [library](https://github.com/discordpp/discordpp))

## Library odds and ends

* [kozet_fixed_point](https://gitlab.com/nagakawa/kozet_fixed_point): A fixed point library for C++.
* [kozet_coroutine](https://gitlab.com/nagakawa/kozet_coroutine): A boopy coroutine library for C and C++, with a pinch of assembly.
* [zekku](https://gitlab.com/nagakawa/zekku): Header-only library including quadtree implementation, for C++.