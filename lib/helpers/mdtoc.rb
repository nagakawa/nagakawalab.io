module MDTOCHelper
  def maketoc(item)
    content = item.compiled_content
    # Get lines that start with '##'
    subsecs = content.lines.keep_if { |line|
      line.start_with?("##")
    }
    i = -1
    subsecs.map! { |line|
      name = line[2..-1].strip # Get name of sec itself
      name = name.gsub(/\[([^\]]*)\]\([^)]*\)/, '\1')
      i += 1
      "%d [%s](#entry%d)" % [i, name, i]
    }
    return subsecs.join("  \n")
  end
  def makebody(item)
    content = item.compiled_content
    # Get lines that start with '##'
    i = -1
    lines = content.lines.map! { |line|
      line = line.chomp
      if line.start_with?("##") then
        i += 1
        "%s {#entry%d}" % [line, i]
      else
        line
      end
    }
    return lines.join("\n")
  end
end
